# Event listener

Read this in another language | [English](/README.md) | [Русский](/docs/ru/README.md)
|---|---|---|

## Quick Links

[Changelog](/changelog.txt)
|---|

## Contents

* [Overview](#overview)
* [Future plans](#plans)
* [Issues](#issue)
* [Features](#feature)
* [Installing](#installing)
* [License](#license)

## Overview

The mod combine events of other scripts and adding new manipulation with events (such like canceling an event). Designed for mod developers. There is an example in the mod. Don't forget about dependencies.

## <a name="plans"></a> Future plans

* New customization for events

## <a name="issue"></a> Found an Issue?

Please report any issues or a mistake in the documentation, you can help us by [submitting an issue][issues] to our GitLab Repository or on [mods.factorio.com][mod portal] or on [forums.factorio.com][homepage].

## <a name="feature"></a> Want a Feature?

You can *request* a new feature by [submitting an issue][issues] to our GitLab Repository or on [mods.factorio.com][mod portal] or on [forums.factorio.com][homepage].

## Installing

If you have downloaded a zip archive:

* simply place it in your mods directory.

For more information, see [Installing Mods on the Factorio wiki](https://wiki.factorio.com/index.php?title=Installing_Mods).

If you have downloaded the source archive (GitLab):

* copy the mod directory into your factorio mods directory
* rename the mod directory to event-listener_*versionnumber*, where *versionnumber* is the version of the mod that you've downloaded (e.g., 0.8.3)

## License

This project is copyright © 2019 ZwerOxotnik \<zweroxotnik@gmail.com\>.

Use of the source code included here is governed by the MIT licence. See the [LICENCE](/LICENCE) file for details.

[issues]: https://gitlab.com/ZwerOxotnik/event-listener/issues
[mod portal]: https://mods.factorio.com/mod/event-listener/discussion
[homepage]: https://forums.factorio.com/viewtopic.php?f=190&t=64621
[Factorio]: https://factorio.com/
