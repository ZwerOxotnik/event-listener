--[[
Copyright (c) 2019 ZwerOxotnik <zweroxotnik@gmail.com>
Licensed under the MIT licence;
Author: ZwerOxotnik
Version: 0.8.1 (2019-05-03)
Description: The script combine events of other scripts
             and adding new manipulation with events (such like canceling an event).
             Designed for mod developers.

You can attach this file via \/
event_listener = require("__event-listener__/branch-2/v0-8-1")
and turn on the "Event listener" mod.
If you want to attach the file in a mod/scenario, then don't forget about depencies in your info.json (https://wiki.factorio.com/Tutorial:Modding_FAQ)
OR copy and paste this file in any project/scenario for handling events.

You can write and receive any information on the links below.
Source: https://gitlab.com/ZwerOxotnik/event-listener
Mod portal: https://mods.factorio.com/mod/event-listener
Homepage: https://forums.factorio.com/viewtopic.php?f=190&t=64621

]]--

local max_standard_id = 0
local events = {}
for name, id in pairs( defines.events ) do
	if id > max_standard_id then max_standard_id = id end
	events[id] = name
end

local SCRIPT_EVENTS_FOR_CHECKING = {
	on_init = true,
	on_configuration_changed = true,
	on_load = true
} -- boolean type needed for future updates

local debug_mode = false
local registered_modules
local standart_events
local script_events
local custom_events
local module = {}
module.version = "0.8.1"

local function new_log(message)
	if settings.global["EL_logs-mode"] and not settings.global["EL_logs-mode"].value then return end

	log(message)
	if game then
		game.write_file("event_listener", message, true)
	end
end

local function undefined(name)
	local message = "Can't identify the '" .. name .. "' event"
	new_log(message)
	if settings.global["EL_debug-mode"] and settings.global["EL_debug-mode"].value then
		error(message)
	end
end

-- Check and get events from modules for handling
local function handle_events(modules)
	standart_events = {}
	custom_events = {}
	script_events = {}
	-- Find events from modules
	for name_mod, data in pairs( modules ) do
		if data.version then
			new_log("Checking events of '" .. name_mod .. "' version='" .. data.version .. "'")
		else
			new_log("Checking events of '" .. name_mod .. "' version='unknown'")
		end

		for k, func in pairs( data.events ) do
			new_log("Adding of event '" .. k .. "'")
			local type = type(k)
			if type == "string" then
				if defines.events[k] then
					if standart_events[k] == nil then standart_events[k] = {} end
					if func ~= function() end then
						table.insert(standart_events[k], func)
					end
				else
					undefined(k)
				end
			elseif type == "number" then
				if k > max_standard_id then
					if custom_events[k] == nil then custom_events[k] = {} end
					if func ~= function() end then
						table.insert(custom_events[k], func)
					end
				else
					local event_name = events[k]
					new_log(k .. " = " .. event_name)
					if standart_events[event_name] == nil then standart_events[event_name] = {} end
					if func ~= function() end then
						table.insert(standart_events[event_name], func)
					end
				end
			else
				undefined(k)
			end
		end

		for event_name, _ in pairs( SCRIPT_EVENTS_FOR_CHECKING ) do
			if script_events[event_name] == nil then script_events[event_name] = {} end
			if data[event_name] and data[event_name] ~= function() end then
				log("yep")
				table.insert(script_events[event_name], data[event_name])
			end
		end
	end
	new_log("Finding events from modules is done")

	-- Attach events of standart_events
	for event_name, _ in pairs( standart_events ) do
		if script.get_event_handler(defines.events[event_name]) == nil then
      new_log("Handled event '" .. event_name .. "'")
			script.on_event(defines.events[event_name], function(event)
				for _, _event in pairs( standart_events[event_name] ) do
					if _event(event) then return end
				end
			end)
		end
	end

	-- Attach events of custom_events
	for event_id, _ in pairs( custom_events ) do
		if script.get_event_handler(event_id) == nil then
      new_log("Handled event '" .. event_id .. "'")
			script.on_event(event_id, function(event)
				for _, _event in pairs( custom_events[event_id] ) do
					if _event(event) then return end
				end
			end)
		end
	end

	-- Attach events of script_events
	for event_name, _ in pairs( script_events ) do
    new_log("Handled event '" .. event_name .. "'")
		script[event_name](function(event)
			for _, _event in pairs( script_events[event_name] ) do
				_event(event)
			end
		end)
	end
	new_log("Handling events from modules is done")
end

-- Handle all possible events from modules for the game
module.add_events = function(modules)
	new_log('Event listener ' .. module.version .. ' adding events, working inside ' .. script.mod_name)

	if type(modules) == 'table' then
		registered_modules = modules
		handle_events(modules)
	else
		local message = 'Type of modules is not table!'
		new_log(message)
		if settings.global["EL_debug-mode"].value then
			error(message)
		end
	end

	if game then
		new_log('Event listener ' .. module.version .. ' finished adding of events during the game. Game tick = ' .. game.tick)
	else
		new_log('Event listener ' .. module.version .. ' finished adding of events before during the game')
	end
end

local function update_standart_event_by_name(event_name)
	standart_events[event_name] = {}
	for name_mod, data in pairs( registered_modules ) do
		local func = data.events[event_name or defines.events[event_name]]
		if func then
			if data.version then
				new_log("Updating '" .. event_name .. "' event of '" .. name_mod .. "' version='" .. data.version .. "'")
			else
				new_log("Updating '" .. event_name .. "' event of '" .. name_mod .. "' version='unknown'")
			end
			if func ~= function() end then
				table.insert(standart_events[event_name], func)
			end
		end
	end

	local event_id = defines.events[event_name]
  if script.get_event_handler(event_id) == nil then
    new_log("Handled event '" .. event_name .. "'")
		script.on_event(event_id, function(event)
			for _, _event in pairs( standart_events[event_name] ) do
				if _event(event) then return end
			end
		end)
	end
end

local function update_standart_event_by_id(id)
	local event_name = events[id]
	if not event_name then undefined(id) return end
	standart_events[event_name] = {}

	for name_mod, data in pairs( registered_modules ) do
		local func = data.events[id or event_name]
		if func then
			if data.version then
				new_log("Updating '" .. event_name .. "' event of '" .. name_mod .. "' version='" .. data.version .. "'")
			else
				new_log("Updating '" .. event_name .. "' event of '" .. name_mod .. "' version='unknown'")
			end
			if func ~= function() end then
				table.insert(standart_events[event_name], func)
			end
		end
	end

	if script.get_event_handler(id) == nil then
    new_log("Handled event '" .. event_name .. "'")
		script.on_event(id, function(event)
			for _, _event in pairs( standart_events[event_name] ) do
				if _event(event) then return end
			end
		end)
	end
end

local function update_custom_event_by_id(id)
	custom_events[id] = {}
	for name_mod, data in pairs( registered_modules ) do
		if data.events[id] then
			if data.version then
				new_log("Updating '" .. id .. "' event of '" .. name_mod .. "' version='" .. data.version .. "'")
			else
				new_log("Updating '" .. id .. "' event of '" .. name_mod .. "' version='unknown'")
			end
			if data.events[id] ~= function() end then
				table.insert(custom_events[id], data.events[id])
			end
		end
	end

	if script.get_event_handler(id) == nil then
    new_log("Handled event '" .. id .. "'")
		script.on_event(id, function(event)
			for _, _event in pairs( custom_events[id] ) do
				if _event(event) then return end
			end
		end)
	end
end

-- Not tested! Probably, high chance of desync
local function update_script_event(k)
	script_events[k] = {}
	for name_mod, data in pairs( registered_modules ) do
		if data.version then
			new_log("Updating events of '" .. name_mod .. "' version='" .. data.version .. "'")
		else
			new_log("Updating events of '" .. name_mod .. "' version='unknown'")
		end
		if data.events[k] then
			if data.events[k] ~= function() end then
				table.insert(script_events[k], data.events[k])
			end
		end
	end

  -- new_log("Handled event '" .. k .. "'")
  -- script[k](function(e)
	-- 	for _, _event in pairs( script_events[k] ) do
	-- 		_event(e)
	-- 	end
	-- end)
end

module.update_event = function(event)
	if game then
		new_log("Event listener " .. module.version .. " updating '" .. event .. "' event during the game, working inside '" .. script.mod_name .. "'. Game tick = " .. game.tick)
	else
		new_log("Event listener " .. module.version .. " updating '" .. event .. "' event before during the game, working inside " .. script.mod_name)
	end
	if type(event) == "string" then
        if defines.events[event] then
			update_standart_event_by_name(event)
		elseif SCRIPT_EVENTS_FOR_CHECKING[event] then
			update_script_event(event)
		end
	elseif type(event) == "number" then
		if event > max_standard_id then
			update_custom_event_by_id(event)
		else
			update_standart_event_by_id(event)
		end
	end
end

module.update_events = function()
	if game then
		new_log("Event listener " .. module.version .. " updating events during the game, working inside '" .. script.mod_name .. "'. Game tick = " .. game.tick)
	else
		new_log("Event listener " .. module.version .. " updating events before during the game, working inside " .. script.mod_name)
	end

	if type(registered_modules) == 'table' then
		handle_events(registered_modules)
	else
		local message = "Type of registered_modules is not table! Type of registered_modules='" .. type(registered_modules) .. "'"
		new_log(message)
		if settings.global["EL_debug-mode"].value then
			error(message)
		end
	end

	if game then
		new_log('Event listener ' .. module.version .. ' finished updating of events during the game, working inside ' .. script.mod_name .. '. Game tick = ' .. game.tick)
	else
		new_log('Event listener ' .. module.version .. ' finished updating of events before during the game, working inside ' .. script.mod_name)
	end
end

-- This function only for embedded version in another place
module.set_debug_mode = function(bool)
	if settings.global["EL_debug-mode"] then new_log("Use the mod settings instead of set_debug_mode!") end

	if game then
		new_log("Temporarily 'Event listener' " .. module.version .. " does not support changing debug mode during the game")
	else
		debug_mode = bool
	end
end

return module
